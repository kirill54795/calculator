﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class AdditionOperation : Operation
    {
        public AdditionOperation(int firstInput, int secondInput) : base(firstInput, secondInput)
        {
        }

        public override bool Calculate(out int result)
        {
            result = _x + _y;

            return true;
        }
    }
}
