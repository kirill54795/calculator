﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {

            int firstNumber = 0;
            int secondNumber = 0;
            int result = 0;
            string operation = "";
            string parsePattern = @"\d+|\W";
            string inputString = " ";

            bool firstInput = false;

            //operation = "My name is Kirill\n" +
            //           "and, today, I am twenty-six years old.\n" +
            //           "I love basketball\n";     
            //Console.WriteLine(operation);
            inputString = "10259+2-53*999/777";
            
            
            bool check = String.IsNullOrEmpty(inputString);
            if (check == true)
            { Console.WriteLine("Enter your values and operation for it."); }
            //inputString = Console.ReadLine();

            Regex regex = new Regex(parsePattern);
            var arrayInputs = regex.Matches(inputString);

            for (int i=0; i <= arrayInputs.Count; i++)
            {
                bool checkFirstNumber = Int32.TryParse(Convert.ToString(arrayInputs[i]), out firstNumber);
                if (checkFirstNumber == false)
                {
                    operation = Convert.ToString(arrayInputs[i]);
                    bool checkSecondNumber = Int32.TryParse(Convert.ToString(arrayInputs[i+1]), out secondNumber);
                }
                
            }

            while (!firstInput)
            {
                Console.WriteLine("Enter first value");
                firstInput = Int32.TryParse(Console.ReadLine(), out firstNumber);
            }
           
            bool secondInput = false;

            while (!secondInput)
            {
                Console.WriteLine("Enter second value");
                secondInput = Int32.TryParse(Console.ReadLine(), out secondNumber);
            }

            Dictionary<string, Operation> operations = new Dictionary<string, Operation>();

            var addition = new AdditionOperation(firstNumber,secondNumber);

            var logger = new LogManager();

            addition.Calculated += logger.MyMethod;

            operations.Add("+", new AdditionOperation(firstNumber, secondNumber));
            operations.Add("-", new SabtractionOperation(firstNumber, secondNumber));
            operations.Add("*", new MultiplicationOperation(firstNumber, secondNumber));
            operations.Add("/", new DivisionOperation(firstNumber, secondNumber));

            while (!operations.ContainsKey(operation))
            {
                Console.WriteLine("Enter your operation");
                operation = Console.ReadLine();
            }

            int j;
            Console.WriteLine("Your result {0}", operations[operation].Calculate(out j));

        }
    }
}
