﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class SabtractionOperation : Operation
    {
        public SabtractionOperation(int firstInput, int secondInput) : base(firstInput, secondInput)
        {
        }

        public override bool Calculate(out int result)
        {
            base.Calculate(out result);

            result = _x - _y;

            return true;
        }
    }

    public class FakeOperation : Operation
    {
        public FakeOperation(int firstInput, int secondInput) : base(firstInput, secondInput)
        {
        }


    }
}
