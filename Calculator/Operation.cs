﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public abstract class Operation
    {
        protected int _x;
        protected int _y;

        
        protected Operation (int firstInput, int secondInput)
        {
            _x = firstInput;
            _y = secondInput;
        }

        public virtual bool Calculate(out int result)
        {
            if (Calculated != null)
            {
                Calculated(this, EventArgs.Empty);
            }

            result = 0;

            return false;
        }

        public event EventHandler Calculated;


    }

}   
