﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class MultiplicationOperation : Operation
    {
        public MultiplicationOperation(int firstInput, int secondInput) : base(firstInput, secondInput)
        {
        }

        public override bool Calculate(out int result)
        {
            result = _x * _y;

            return true;
        }
    }
}
