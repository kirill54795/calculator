﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class LogManager
    {
        public void MyMethod(object i, EventArgs args)
        {
            Console.WriteLine("This type generated event {0}", i.ToString());
        }
    }
}
