﻿using System;
using System.Text;
using System.Collections.Generic;
using Calculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestCalculator
{
    /// <summary>
    /// Summary description for SabtractionTest
    /// </summary>
    [TestClass]
    public class SabtractionTest
    {
        [TestMethod]
        public void Minus()
        {
            int i;
            SabtractionOperation minus = new SabtractionOperation(8,4);
            bool result = minus.Calculate(out i);
            Assert.AreEqual(4, i);
        }
    }
}
