﻿using System;
using System.Text;
using System.Collections.Generic;
using Calculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestCalculator
{
    /// <summary>
    /// Summary description for SabtractionTest
    /// </summary>
    [TestClass]
    public class DivisionTest
    {
        [TestMethod]
        public void division()
        {
            int i;
            DivisionOperation div = new DivisionOperation(8,4);
            bool result = div.Calculate(out i);
            Assert.AreEqual(2, i);
        }
    }
}
