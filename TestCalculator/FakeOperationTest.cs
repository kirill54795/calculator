﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestCalculator
{
    [TestClass]
    public class FakeOperation
    {
        [TestMethod]
        public void Fake()
        {
            Calculator.FakeOperation fake = new Calculator.FakeOperation(0,200);

            int i;

            var actual = fake.Calculate(out i);
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void FakeOutDefaultTest()
        {
            Calculator.FakeOperation fake = new Calculator.FakeOperation(0,200);

            int i;

            var actual = fake.Calculate(out i);
            Assert.AreEqual(i, 0);
        }

    }
}
