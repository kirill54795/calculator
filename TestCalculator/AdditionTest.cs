﻿using System;
using Calculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestCalculator
{
    [TestClass]
    public class AdditionOPerationTest
    {
        [TestMethod]
        public void CalculateTest()
        {
            int i;
            AdditionOperation plus = new AdditionOperation(1, 3);
            bool result = plus.Calculate(out i);
            Assert.AreEqual(4,i);
        }
    }
}
